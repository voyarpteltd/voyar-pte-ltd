At Voyar we are building the FROG up as an open-ended mobility system with many customisable modules and features, which we introduce progressively. The FROG walker is designed to be customised to the needs of the individual and to evolve with them without becoming cumbersome or heavy.

Address: 35 Joo Koon Circle, Singapore 629110

Phone: +65 6861 0889

Website: https://voyarmobility.com
